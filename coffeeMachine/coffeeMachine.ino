#include <LiquidCrystal_I2C.h>
#include <Adafruit_MAX31865.h>

#define DS3231_I2C_ADDRESS 0x68
// The value of the Rref resistor. Use 430.0!
#define RREF 430.0

int boilerCtl = 2;
int counter = 0;

Adafruit_MAX31865 max = Adafruit_MAX31865(10);

// Initialize and setup liquidcrystal I2C library
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
void setup() {
  max.begin(MAX31865_2WIRE);  // set to 2WIRE or 4WIRE as necessary
  Serial.begin(9600);
  lcd.begin(20,4);   // initialize the lcd for 16 chars 2 lines, turn on backlight
  lcd.backlight(); // turn backlight on  

  lcd.setCursor(7,0); //Start at character 4 on line 0
  lcd.print("Gaggia");
  lcd.setCursor(3,1);
  lcd.print("Coffee Machine");
  pinMode(boilerCtl,OUTPUT);
  
}

void loop() {
uint16_t rtd = max.readRTD();

  //Serial.print("RTD value: "); Serial.println(rtd);
  float ratio = rtd;
  ratio /= 32768;
  //Serial.print("Ratio = "); Serial.println(ratio,8);
  //Serial.print("Resistance = "); Serial.println(RREF*ratio,8);
  float temp = max.temperature(100, RREF)-1;
  Serial.print(counter);
  counter++;
  Serial.print(" Sec");
  Serial.print("\t");
  Serial.print(temp);
  Serial.print(" C");
  lcd.setCursor(7,2);
  lcd.print(temp);
  
  if(temp <= 105){
    digitalWrite(boilerCtl,HIGH);
  }else {
    digitalWrite(boilerCtl,LOW);
  }
  // Check and print any faults
  uint8_t fault = max.readFault();
  if (fault) {
    Serial.print("Fault 0x"); Serial.println(fault, HEX);
    if (fault & MAX31865_FAULT_HIGHTHRESH) {
      Serial.println("RTD High Threshold"); 
    }
    if (fault & MAX31865_FAULT_LOWTHRESH) {
      Serial.println("RTD Low Threshold"); 
    }
    if (fault & MAX31865_FAULT_REFINLOW) {
      Serial.println("REFIN- > 0.85 x Bias"); 
    }
    if (fault & MAX31865_FAULT_REFINHIGH) {
      Serial.println("REFIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_RTDINLOW) {
      Serial.println("RTDIN- < 0.85 x Bias - FORCE- open"); 
    }
    if (fault & MAX31865_FAULT_OVUV) {
      Serial.println("Under/Over voltage"); 
    }
    max.clearFault();
  }
  Serial.println();
  delay(1000);
}
