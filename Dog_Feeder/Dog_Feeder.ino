
//////////////////////////////////////////////////////////////////////////////////////////
//                                   Libraries                                          //
//////////////////////////////////////////////////////////////////////////////////////////                             

#include <Wire.h>                   //iic wire library
#include <RTClib.h>                 //rtc clock library
#include <LiquidCrystal_I2C.h>      //iic serial library
#include <Servo.h>                  //servo library

/////////////////////////////////////////////////////////////////////////////////////////
//                                  Objects                                            //
/////////////////////////////////////////////////////////////////////////////////////////

RTC_DS1307 RTC;

////////////////////////////////////////////////////////////////////////////////////////
//                          Initialization and Assigment                              //
////////////////////////////////////////////////////////////////////////////////////////

LiquidCrystal_I2C lcd(0x27, 16, 2);  // Set the LCD I2C address

Servo servo;                      //assign servo library to servo function

const int servoPin = 9;           //assign variable instance to pin 3

////////////////////////////////////////////////////////////////////////////////////////
//                                        Setup                                       //
////////////////////////////////////////////////////////////////////////////////////////

void setup () {
    servo.attach(servoPin);       //Initialize servo library and attach variable servo pin and assign pin number
    Serial.begin(9600);           //begin serial @ 9600 baud
    lcd.begin(16,2);              //Initialize lcd 16 Char * 2 Char high
    lcd.backlight();              //set lcd backlight to on (only with serial iic lcd)
    Wire.begin();                 //Initialize wire library for lcd and rtc clock
    lcd.setCursor(2,0);           //Set cursor position on lcd
    lcd.print("Dog Feeder");      //Print to lcd screen "Dog Feeder"
    delay(1000);
    lcd.setCursor(0,1);           //Set cursor position on lcd
    RTC.begin();                  //Initialize RTC Clock

    //prints to the serial console if RTC clock isnt functioning
  if (! RTC.isrunning()) {
    Serial.println("RTC is NOT running!");
    // following line sets the RTC to the date & time this sketch was compiled
    RTC.adjust();
  }
}

///////////////////////////////////////////////////////////////////////////////////////
//                                 Main Program                                      //
///////////////////////////////////////////////////////////////////////////////////////

void loop () {
    
    DateTime now = RTC.now(); 
    Serial.print(now.year(), DEC);
    Serial.print('/');
    Serial.print(now.month(), DEC);
    Serial.print('/');
    Serial.print(now.day(), DEC);
    Serial.print(' ');
    Serial.print(now.hour(), DEC);
    Serial.print(':');
    Serial.print(now.minute(), DEC);
    Serial.print(':');
    Serial.print(now.second(), DEC);
    Serial.println(); 
    delay(1000);
    if(now.hour() == 9 && now.minute() == 00 && now.second() == 00){
    servo.write(0);
    delay(2000);
    servo.write(90);
  }else if (now.hour() == 18 && now.minute() == 00 && now.second() == 00)
  {
    servo.write(0);
    delay(2000);
    servo.write(90);
  }
}

