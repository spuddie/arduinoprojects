#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

const char* ssid = "test2";     //enter wifi ssid here
const char* pw = "22345678";    //enter wifi password here
int doorTrigger = 5;
int sirenOutput = 4;
int sirenActive = 0;
int magnetPosition1;
int magnetPosition2;
int alarmState;       
int alarmActiveLed = 14;
int alarmInactiveLed = 12;
byte incomingPacket;

//WiFiServer Server(80);
WiFiUDP Server;

void setup() {
  pinMode(doorTrigger, INPUT);
  pinMode(sirenOutput, OUTPUT);
  pinMode(alarmActiveLed, OUTPUT);
  pinMode(alarmInactiveLed, OUTPUT);
  
  Serial.begin(9600);
  //setup AP
  WiFi.mode(WIFI_AP);
  WiFi.softAP(ssid,pw);
  WiFi.begin();
  Serial.println("AP started at: ");
  Serial.print(WiFi.softAPIP());
  Server.begin(80);
  Serial.println("Server started...");
 
}
void sirenAlarm()
{
  digitalWrite(sirenOutput, HIGH);
  delay(300000);
  digitalWrite(sirenOutput, LOW);
    
}

void loop() 
{
  byte messageFromClient = Server.parsePacket(); //variable to check if a packet is being sent
  
    if (messageFromClient)// check for an incoming packet(or messageFromClient == true)
    {
      incomingPacket = Server.read();//initialize a variable that recieves a byte of data from a packet('y' or 'n')
      //This code will be replaced with a future lcd upgrade
      /*This code is to notify the person who is arming or disarming
      the alarm that the alarm was successfully activated or 
      deactivated by sound*/
      if(incomingPacket == 'y')//check for the incoming packet y
      { //chirp the alarm twice to notify the user that the alarm is active
        digitalWrite(sirenOutput, HIGH);
        delay(50);
        digitalWrite(sirenOutput, LOW);
        delay(250);
        digitalWrite(sirenOutput, HIGH);
        delay(50);
        digitalWrite(sirenOutput, LOW);
      } else if(incomingPacket == 'n')
        { //chirp the alarm once to notify the user that the alarm is deactive
          digitalWrite(sirenOutput, HIGH);
          delay(50);
          digitalWrite(sirenOutput, LOW);
        }
    } 
      //activate alarm when valid packet is recieved from client
      if(incomingPacket == 'y')
      {
        alarmState = 1;   //set alarm state to high
        if(alarmState == 1)
        { //write doortrigger state into two variables               
          magnetPosition1 = digitalRead(doorTrigger);
          delay(20);
          magnetPosition2 = digitalRead(doorTrigger);//stops false triggers
            //activates siren if door trigger state is high
            if(magnetPosition1 == HIGH && magnetPosition2 == HIGH && sirenActive == 0)
            {
              sirenActive = 1;
              sirenAlarm();
            } 
        } 
          digitalWrite(alarmInactiveLed, LOW);// led that indicates alarm state
          digitalWrite(alarmActiveLed, HIGH);//led that indicates alarm state
        }
        //deactivate alarm when valid packet "n" is recieved
        else if (incomingPacket == 'n')
         {
          sirenActive = 0;
          digitalWrite(alarmActiveLed, LOW);//led that indicates alarm state
          digitalWrite(alarmInactiveLed, HIGH);//led that indicates alarm state
         }    
  delay(250);
  
}
