////////////////////////////////////////////////////////////////////
//                           LIBRARIES                            //
////////////////////////////////////////////////////////////////////

#include <Wire.h>  // Comes with Arduino IDE
#include <LiquidCrystal.h>
#include <Servo.h>

////////////////////////////////////////////////////////////////////
//               DEFINES AND VARIABLE DECLARATION                 //
////////////////////////////////////////////////////////////////////

#define DS3231_I2C_ADDRESS 0x68

// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val)
{
  return( (val/10*16) + (val%10) );
}

// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val)
{
  return( (val/16*10) + (val%16) );
}

//initialize lcd display and assign pins
LiquidCrystal lcd(12,11, 5, 4, 3, 2); 

// set constant variables for the input and output pins
const int servoPin = 8;

//assign servo library to servo function
Servo servo;

////////////////////////////////////////////////////////////////////
//                              SETUP                             //
////////////////////////////////////////////////////////////////////

void setup()
{
   //Initialize servo and set pin for servo to pin 8
  servo.attach(servoPin);

  //set character lcd type
  lcd.begin(16,2);  

//-------- Write characters on the display ------------------
 
  lcd.setCursor(2,0); //Start at character 4 on line 0
  lcd.print("Animal Feeder");
  delay(1000);

  // initialize rtc module
  Wire.begin();
  setDS3231time(00,00,00);
}

//set time function
void setDS3231time(byte second, byte minute, byte hour)
{
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second)); // set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.endTransmission();
}

//read time function
void readDS3231time(byte *second,byte *minute,byte *hour)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);
  // request seven bytes of data from DS3231 starting from register 00h
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  }
   
   byte second, minute, hour;

//display time function
void displayTime()
{
  // retrieve data from DS3231
  readDS3231time(&second, &minute, &hour);
  // send it to the serial monitor
  lcd.print(hour, DEC);
  // convert the byte variable to a decimal number when displayed
  lcd.print(":");
  if (minute<10)
  {
    lcd.print("0");
  }
  lcd.print(minute, DEC);
  lcd.print(":");
  if (second<10)
  {
    lcd.print("0");
  }
  lcd.print(second, DEC);
  lcd.print(" ");
  
    
}


void loop()
{
  //set cursor position to the middle of the display
  lcd.setCursor(4,1);
  displayTime(); // display the real-time clock data on the Serial Monitor,
   
  if(hour == 00 && minute == 00 && second == 2){
    servo.write(0);
    delay(750);
    servo.write(180);
    delay(750);
    servo.write(0);
    delay(750);
    servo.write(180);
    delay(750);
    servo.write(90);
  }else if (hour == 8 && minute == 00 && second == 00)
  {
    servo.write(90);
    delay(1000);
    servo.write(90);
  }else if (hour == 16 && minute == 00 && second == 00)
  {
    servo.write(90);
    delay(1000);
    servo.write(90);
  }
 
}

