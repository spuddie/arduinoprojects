/*  
 *   By Nathan Collins
 *   Electric Fence for Pets
 *   
 *   The program is based on an Arduino Nano. 
 *   It will read 2 potentimeters, 1 to set 
 *   the pulse length and 1 to set the percentage 
 *   of on time during each pulse length. 
 *   The output is connected to a mosfet controlled 
 *   HV ignition coil, that will provide high voltage 
 *   to the electrical fence to keep animals inside a certain 
 *   area and to keep other animals out.   
 *   The power output of the HV ignition coil is controlled via 
 *   the amount of percentage the coil is on every pulse length.
 *   
 *   ## Possible Future Upgrades ##
 *   
 *   - Oled high contrast large viewing angle display to show
 *     pulse rate fence status.
 *   
 *   - Outdoor temperature humidity and pressure sensor, its readings 
 *     will be displayed on the oled display.
 *   
 *   - Fence fault indicator (fence voltage meter).  Reading displayed 
 *     via oled display or via a set of leds.
 */


const int outputPin = 10;   // define pin number 
int dwellTimeInterval = 0;  // Variable for percentage of fence on time
bool timerState = false;  // Variable to determine fence on/off status
unsigned long previousMillis = 0;  // Variable for pulse and dwell timing

void setup() {
   // Initialize the output pin as a output
   pinMode(outputPin, OUTPUT);
}

// read potentimeter value and write into pulseTime Variable
int pulseTime = analogRead(A2);
// read potentimeter value and write into dwellTime Variable
int dwellTime = analogRead(A3);
  

void loop() {

  // Convert the Pulse timer variable data (0-1023)
  // to millisecond friendly numbers 0ms to 3000ms. 
  pulseTime = map(pulseTime, 0, 1023, 0, 3000);

  // Do some maths to fiqure out the dwell time or percentage of on time 
  dwellTimeInterval = ( pulseTime / 1023 ) *  dwellTime;

  // Start the millis timer and write to a variable
  unsigned long currentMillis = millis();

  // Check timerstate if false turn on fence and change timerstate
  // Check if the dwell time or pulse time potentimeter has been changed
  if ( timerState == false ) {
    digitalWrite(outputPin, HIGH);
    timerState = true;
    if ( pulseTime >= pulseTime + 100 || pulseTime <= pulseTime - 100 ) {
      pulseTime = analogRead(A2);
    } else if ( dwellTime >= dwellTime + 50 || dwellTime <= dwellTime - 50 ) {
      dwellTime = analogRead(A3);
    }
  }
  // Check if the dwell time has finish if so turn of electric fence
  if ( currentMillis - previousMillis >= dwellTimeInterval ) {
    digitalWrite(outputPin, LOW);
  }
  // Check if pusle time has finished reset timerstate
  if ( currentMillis - previousMillis >= pulseTime ) {
    timerState = false;
  }
  
}
