/* Animal Feeder
 *   
 *   Activates and deactivates a continuous rotational servo, it turns a  
 *   feeder to dispense dry dog food.  This is done at preselected times,
 *   by means of a RTC module which keeps track of the time and dispalys
 *   it on a character lcd. 
 *   
 *   Future Improvements
 *     - Button debounce without using delay
 *     - Countdown timer until next feed
 *     - Possible upgrade to camera with raspberry pi 
 *     - Possible sense collars for the cats
 *     - Possible upgrade of code if/else to functions or switch statements
 *     - Clean up messy code
 *     
 *   Created by Nathan Collins
 */

////////////////////////////////////////////////////////////////////
//                 Libraries and Defines.....                     //  
////////////////////////////////////////////////////////////////////

#include <Wire.h>  // Comes with Arduino IDE
#include <LiquidCrystal_I2C.h>

////////////////////////////////////////////////////////////////////
// * MAKE SURE YOU USE NewliquidCrystal_1.3.4 LIBRARY             //
// https://bitbucket.org/fmalpartida/new-liquidcrystal/downloads/ //
////////////////////////////////////////////////////////////////////

#include <Servo.h>
#define DS3231_I2C_ADDRESS 0x68

// Convert normal decimal numbers to binary coded decimal
byte decToBcd(byte val)
{
  return( (val/10*16) + (val%10) );
}
// Convert binary coded decimal to normal decimal numbers
byte bcdToDec(byte val)
{
  return( (val/16*10) + (val%16) );
}

// Initialize and setup liquidcrystal I2C library
LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

/*
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 * !!PIN 3 FOR DOG FEEDER, PIN 9 FOR CAT FEEDER!! 
 * !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
 */
const int servoPin = 3; // Declare variable for servoPin (attach to digital pin 3 or pin 9)

//  THIS IS FOR THE CAT FEEDER ONLY COMMENT OUT WHEN NOT USING IT
//const int feedButton = 2;  // Declare variable for manual button can feed animal manually

/////////////////////////////////////////////////////////////////////
// CHANGE INTERVAL VARIABLE TO CHANGE THE AMOUNT OF FOOD DISPENSED //
//                USE VALUE 1000 FOR DOG FEEDER                    //
//
/////////////////////////////////////////////////////////////////////

const long interval = 600; // Variable for interval, the length the servo will be in a certain state
unsigned long previousMillis = 0; // will store the last time the servo state was checked
int buttonState = 0;  // This is for checking button status
int buttonDebounce = 0;  // This is for eliminating button bounce
bool servoState = false;

// assign servo library to servo function
Servo servo;

////////////////////////////////////////////////////////////////////
//                             Setup                              //
////////////////////////////////////////////////////////////////////
 
void setup()
{
  // Initialize servo and set pin for servo to pin 3
  servo.attach(servoPin);
 // pinMode(feedButton, INPUT);  // Assign feeButton to Digital pin 2
  Serial.begin(9600);  // Initialize Serial library
  lcd.begin(16,2);   // initialize the lcd for 16 chars 2 lines, turn on backlight
  lcd.backlight(); // turn backlight on  

//-------- Write characters on the display ------------------

// NOTE: Cursor Position: (CHAR, LINE) start at 0  
  lcd.setCursor(2,0); //Start at character 4 on line 0
  lcd.print("Animal Feeder");
  delay(500);
  lcd.setCursor(0,1);
  Wire.begin();  // Initialize Wire Library
  setDS3231time(00,00,00);// Set Initial time for RTC module
}

////////////////////////////////////////////////////////////////////
//                            Functions                           //
////////////////////////////////////////////////////////////////////
                             
void setDS3231time(byte second, byte minute, byte hour)
{
  // sets time and date data to DS3231
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set next input to start at the seconds register
  Wire.write(decToBcd(second));// set seconds
  Wire.write(decToBcd(minute)); // set minutes
  Wire.write(decToBcd(hour)); // set hours
  Wire.endTransmission();
}

void readDS3231time(byte *second,
byte *minute,
byte *hour)
{
  Wire.beginTransmission(DS3231_I2C_ADDRESS);
  Wire.write(0); // set DS3231 register pointer to 00h
  Wire.endTransmission();
  Wire.requestFrom(DS3231_I2C_ADDRESS, 7);

  // request seven bytes of data from DS3231 starting from register 00h
  *second = bcdToDec(Wire.read() & 0x7f);
  *minute = bcdToDec(Wire.read());
  *hour = bcdToDec(Wire.read() & 0x3f);
  }
   byte second, minute, hour;

void displayTime()
{
  // retrieve data from DS3231
  readDS3231time(&second, &minute, &hour);
  
  // send it to the serial monitor
  lcd.print(hour, DEC);
  
  // convert the byte variable to a decimal number when displayed
  lcd.print(":");
  if (minute<10)
  {
    lcd.print("0");
  }
  lcd.print(minute, DEC);
  lcd.print(":");
  if (second<10)
  {
    lcd.print("0");
  }
  lcd.print(second, DEC);
  lcd.print(" ");
}

////////////////////////////////////////////////////////////////////
//                        Main Program                            //
////////////////////////////////////////////////////////////////////

void loop()
{
  // set cursor so time is displayed in the center of the screen
  lcd.setCursor(4,1);  
  
  // display the real-time clock data on the Serial Monitor
  displayTime(); 
  
  // starts millis function and updates continuously the currentMillis variable
  unsigned long currentMillis = millis();
  
  // Checks RTC time if the scecified time is reached the servo is actived and starts rotating
 // buttonState = digitalRead(feedButton);
  
  //buttonBounce = digitalRead(feedButton);
  /*if(buttonState == HIGH)
  {
    servo.write(180);  // This is for a continuous rotating servo this starts the servo rotating
    previousMillis = millis();  // Write the current millis count into previousMillis
  }else*/ if(hour == 00 && minute == 00 && second == 10 && servoState == false)
   {
       servo.write(180); // Same as above 
       servoState = true;
       previousMillis = millis(); // Same as above
  }else if(hour == 07 && minute == 59 && second == 10)
   {
       servo.write(180); // Same as above
       previousMillis = millis(); // Same as above
  }else if(hour == 15 && minute == 59 && second == 10)
   {
       servo.write(180); // Same as above
       previousMillis = millis(); // Same as above
  }/*else if(hour == 11 && minute == 59 && second == 00 && servoState == false)
   {
       servo.write(180); // Same as above
       previousMillis = millis(); // Same as above
  }else if(hour == 11 && minute == 59 && second == 10)
   {
       servo.write(180); // Same as above
       previousMillis = millis(); // Same as above
  }/*else if(hour == 19 && minute == 59 && second == 00)
    {
       servo.write(180); // Same as above
       previousMillis = millis(); // same as above 
    }*/
  
   // this statement checks if the interval time period has elapsed, if so the continuous
   // rotating servo is stopped
  if (currentMillis - previousMillis >= interval)
  {
    servo.write(90);
    servoState = false;
  }
 }
