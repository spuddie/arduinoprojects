/*
 *   MAKE SURE TO USE THE ESP8266-I2C-LCD1602-master/ LIBRARY
 *   CHECK THAT THE CORRECT LIBRARY IS IN THE LIBRARIES FOLDER 
 *   FOR THE ARDUINO IDE
 *   Made by Nathan Collins spuddie@bitbucket.org
 *   
 */

#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
#include <SPI.h>  
#include <MFRC522.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#define SS_PIN 15  //set pin 15 as spi chip select pin 
#define RST_PIN 5  //reset pin for spi interface 

// Set the LCD address to 0x27 for a 16 chars and 2 line display
LiquidCrystal_I2C lcd(0x27, 16, 2);
WiFiUDP Client;                     //Initialize Udp interface
MFRC522 mfrc522(SS_PIN, RST_PIN);   // Create MFRC522 instance.

int alarmActiveLed = 1;       //set pin for alarm active green led
int alarmInactiveLed = 3;     //set pin for alarm inactive red led
boolean stateOfAlarm = false;
char sendState;               //initialize variable for sending a byte 'y' or 'n'

void setup()
{
  // initialize the LCD
  lcd.begin(0,2);  // sda=0 | D3, scl=2 | D4
  lcd.backlight();// Turn on the blacklight
  lcd.print("Welcome");
  pinMode(alarmActiveLed, OUTPUT);
  pinMode(alarmInactiveLed, OUTPUT);
  Serial.begin(9600);     //initiate serial 
  WiFi.mode(WIFI_STA);      //set the esp8266 wifi to station mode
  WiFi.begin("test2","22345678");     //enter esp8266 wifi and password here...Wifi.begin(wifi name, password)
  SPI.begin();      // Initiate  SPI bus
  mfrc522.PCD_Init();   // Initiate MFRC522
  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi... ");
  lcd.clear();
  lcd.print("Wait for Wifi...");

  //wait for wifi signal from AP
  while(WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
  }
    Serial.println("");
    Serial.println("WiFi connected");
    lcd.clear();
    lcd.print("WiFi connected");
    Serial.println("IP address: ");
    lcd.clear();
    lcd.print("IP address: ");
    lcd.setCursor(3,1);
    lcd.print(WiFi.localIP());
    Serial.println(WiFi.localIP());     //send the stations ip to the serial display   
    Client.begin(81);       //initialize port for recieving packets from AP
    delay(1000);
}

//function for sending udp packets
void udpsend(char packetState)
  {
  const char ip[]="192.168.4.1";
  Client.beginPacket(ip,80);
  Client.write(packetState);
  Client.endPacket();
  delay(500);
  return;
  }

//function for recieving udp packets
void udprecieve()
{
  const char ip[] = "192.168.4.1";
  Client.beginPacket(ip,81);
  Client.read();
  Client.endPacket();
  delay(500);
  return;
}
void loop()
{
  lcd.noBacklight();
  lcd.clear();  //  Clear lcd display
const int port = 80;

  // Look for new cards
  if ( ! mfrc522.PICC_IsNewCardPresent()) 
  {
    return;
  }
  // Select one of the cards
  if ( ! mfrc522.PICC_ReadCardSerial()) 
  {
    return;
  }
  //Show UID on serial monitor
  Serial.print("UID tag :");
  String content= "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
     Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");
     Serial.print(mfrc522.uid.uidByte[i], HEX);
     content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
     content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  Serial.println();
  Serial.print("Message : ");
  content.toUpperCase();      //convert UID String to uppercase

  if (stateOfAlarm == true)//checks if alarm is active when a card is presented with the below UID the state of alarm changes,  The alarm state is sent to the alarm unit and the alarm is disarmed
  {
    if(content.substring(1) == "C5 CD 9F B9" || content.substring(1) == "93 98 6E 9A" || content.substring(1) == "B9 34 E7 29" || content.substring(1) == "04 93 38 C9")
    {
    lcd.backlight();  // Turn backlight on
    lcd.setCursor(5,0);
    lcd.print("Alarm");
    lcd.setCursor(2,1);
    lcd.print("Deactivated");
    Serial.println("Authorized access");
    Serial.println();
    stateOfAlarm = false;
    sendState = 'n';
    Serial.println(sendState);
    digitalWrite(alarmActiveLed, LOW);
    digitalWrite(alarmInactiveLed, HIGH);
    udpsend(sendState);
    Serial.println("Inactive");
    delay(1000);
    }else 
     {
      Serial.println("Access Denied");
     } 
   }else if (stateOfAlarm == false)// checks if alarm is deactivated, when a card is presented with the below UID the state of alarm changes  The alarm state is sent to the alarm unit and the alarm is armed
    {
      if(content.substring(1) == "C5 CD 9F B9" || content.substring(1) == "93 98 6E 9A" || content.substring(1) == "B9 34 E7 29" || content.substring(1) == "04 93 38 C9")
      {
      lcd.backlight();  // Turn backlight on
      lcd.setCursor(5,0);
      lcd.print("Alarm");
      lcd.setCursor(5,1);
      lcd.print("Active");
      Serial.println("Authorized access");
      Serial.println();
      stateOfAlarm = true;
      sendState = 'y';
      Serial.println(sendState);
      digitalWrite(alarmInactiveLed, LOW);
      digitalWrite(alarmActiveLed, HIGH);
      udpsend(sendState);
      Serial.print("Active");
      delay(1000);
      }else {
      Serial.println("Access Denied");
      } 
    }
}
